.PHONY: help build server server-with-drafts sync clean zopfli

PNG_SENTINELS:= $(shell find . -path ./public -prune -o -name '*.png' -print | sed 's|\(.\+/\)\(.\+.png\)|\1.\2.zopfli|g')

help: ## Print this help text
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

server: ## Run hugo server
	hugo server

server-with-drafts: ## Run hugo server and include drafts
	hugo server -D

build: public ## Build site (will also compress images using zopfli)

sync: public ## Synchronize built site with server
	rsync -avzr --progress --delete public/ linode:"/var/www/blog/public"

zopfli: $(PNG_SENTINELS) ## Compress new images using zopfli

clean: ## Remove the built directory
	rm -rf public

public: $(PNG_SENTINELS)
	rm -rf public
	hugo

# Zopfli sentinel rule
.%.png.zopfli: %.png
	./zopflipng --prefix="zoop_" $<
	@mv $(dir $<)zoop_$(notdir $<) $<
	@touch $@
