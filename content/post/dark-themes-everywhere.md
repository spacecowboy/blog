
+++
date = "2014-04-07"
draft = false
title = "Dark themes everywhere"
slug = "dark-themes-everywhere"
tags = ['conkeror', 'css', 'dark', 'web']
banner = ""
aliases = ['/dark-themes-everywhere/']
+++


I have come to really appreciate dark themes in the programs that I
use. Be it any of my Android devices or my *real* computers, I prefer
dark themes. In Emacs, it's as easy as `M-x load-theme wombat`. And
wombat is probably my current favourite dark theme. Dark greys
combined with nice shades of blue, green and red. This entire web site
is (at the time of this post) presented in the wombat colors.

Of course, a lot of time is spent in the web browser and I have come
to understand that most web designers don't share my love for the
darkness. Long have I suffered in the depressing and blinding white
backgrounds of the "Web 2.0".

**N O - M O R E**

I have just come into the possession of some writings that magically
destroys the light infestation in my life. Now I can enjoy the dark
majestic background that is `#202020` everywhere.

![Dark conkeror in Google](/images/2014/Apr/dark-conkeror-google.png)

This is naturally possible to do in most browsers but I'll talk about
conkeror specifically. What's most interesting is probably the CSS
so you don't have to write that yourself.

Great thanks to [Scott Jaderholm](http://jaderholm.com/blog/programothesis-18-conkeror-color-theme) for doing all the work.

## Conkeror

[Conkeror](http://conkeror.org) is a web browser based on the same
underlying tech as Firefox. What makes it unique is its Emacs-like interface. It is essentially what you imagine a good browser in Emacs would be like. Like Emacs, tweaks can be made by adding code (javascript) to its configuration files.

By default, it loads all `.js` files in `~/.conkerorrc/`. So what you want to do is download the `color-theme.js` and `site-css`
files/folders from [Scott's repo](https://github.com/scottjad/dotfiles/tree/master/.conkerorrc) and put that in your `.conkerorrc` dir. That's basically it, but a few additional tweaks can be done.

## Fixing GitHub diff colors

GitHub must have updated their styles since Scott's post.  To fix the diff colors, the following works (replace the existing GitHub entry with this):
```javascript
    make_css_data_uri(
        ['.k { font-weight: bold !important; }', // keyword
        'a, h1, code, pre {text-shadow: 0px 0px 0px  black !important; }',
        '.nv { color: #aaa !important; } ', // function name/variable name
        '.s { color: #95e454 !important; } ', // string
        '.c1 { color: #99968b !important; '+
        'font-style: normal !important; }', // comment
        '.gi, .gi * { color: #1AFF84 !important; }', // inserted line
        '.gd, .gd * { color: #ff0080 !important; }' // deleted line
        ], $domains = "github.com")
```

![Dark conkeror GitHub](/images/2014/Apr/dark-conkeror-github.png)

## Enabling dark theme by default

I always want the dark theme enabled by default. This is the main theme function (I removed all themes but the dark one as well):
```javascript
    function global_color_theme(name, key, styles) {
        interactive_cmd = "toggle-" + name + "-mode";
        // Enable by default here
        for(x in styles) {
            register_user_stylesheet(styles[x]);
        }
        color_theme_toggle[name] = true;
        // Add toggle function
        interactive(interactive_cmd, "",
                    function (I) {
                        if (color_theme_toggle[name]) {
                            for(x in styles) {
                                unregister_user_stylesheet(styles[x]);
                            }
    
                            color_theme_toggle[name] = false;
                        } else {
                            for(x in styles) {
                                register_user_stylesheet(styles[x]);
                            }
                            color_theme_toggle[name] = true;
                        }
                    });
        // Add a disable function
        interactive("disable-"+name+"-theme","", function() {
            for(x in styles) {
                unregister_user_stylesheet(styles[x]);
            }
            color_theme_toggle[name] = false;
        });
        // Add an enable function
        interactive("enable-"+name+"-theme","", function() {
            for(x in styles) {
                register_user_stylesheet(styles[x]);
            }
            color_theme_toggle[name] = true;
        });
        // I don't care for a keybinding for this
        //define_key(default_global_keymap, key, interactive_cmd);
    }
```

## Make the Wombat theme even darker

I prefer an even darker background than the one provided.  I have also learned the importance of the `!important` tag and semicolons in css (damn you!!!). This is incidentally the wombat css that I'm using for syntax highlighting here on the site. If you change anything don't forget to make sure the line ends with `!important` (and semicolon in case of multiple attributes) or the css won't take.

The changes are pretty much in the upper section but I re-formatted the file to make it more readable:
```css
    code, pre, code *, pre * {
        /* Code should always be in monospace */
        font-family: DejaVu Sans Mono !important;
        /*color: #f6f3e8 !important;*/
        /*background-color: #202020 !important;*/
        /*font-size: 10pt !important;*/
    }
    
    .linenos * {
        margin-right: 0.5em !important;
    }
    
    .highlight, .highlighttable  {
        background: #303030 !important;
        color: #f6f3e8 !important;
    }
    .hll, .highlight .hll {
        background-color: #ffffcc !important;
    }
    .c, .highlight .c {
        color: #99968b !important;
        font-style: italic !important;
    } /* Comment */
    .err, .highlight .err {
        color: #f6f3e8 !important;
    } /* Error */
    .g, .highlight .g {
        color: #f6f3e8 !important;
    } /* Generic */
    .k, .highlight .k {
        color: #8ac6f2 !important;
    } /* Keyword */
    .l, .highlight .l {
        color: #f6f3e8 !important;
    } /* Literal */
    .n, .highlight .n {
        color: #f6f3e8 !important;
    } /* Name */
    .o, .highlight .o {
        color: #f6f3e8 !important;
    } /* Operator */
    .x, .highlight .x {
        color: #f6f3e8 !important;
    } /* Other */
    .p, .highlight .p {
        color: #f6f3e8 !important;
    } /* Punctuation */
    .cm, .highlight .cm {
        color: #99968b !important;
        font-style: italic !important;
    } /* Comment.Multiline */
    .cp, .highlight .cp {
        color: #e5786d !important;
    } /* Comment.Preproc */
    .c1, .highlight .c1 {
        color: #99968b !important;
        font-style: italic !important;
    } /* Comment.Single */
    .cs, .highlight .cs {
        color: #99968b !important;
        font-style: italic !important;
    } /* Comment.Special */
    .gd, .highlight .gd {
        color: #f6f3e8 !important;
    } /* Generic.Deleted */
    .ge, .highlight .ge {
        color: #f6f3e8 !important;
    } /* Generic.Emph */
    .gr, .highlight .gr {
        color: #f6f3e8 !important;
    } /* Generic.Error */
    .gh, .highlight .gh {
        color: #f6f3e8 !important;
        font-weight: bold !important;
    } /* Generic.Heading */
    .gi, .highlight .gi {
        color: #f6f3e8 !important;
    } /* Generic.Inserted */
    .go, .highlight .go {
        color: #808080 !important;
        background-color: #202020 !important;
    } /* Generic.Output */
    .gp, .highlight .gp {
        color: #f6f3e8 !important;
    } /* Generic.Prompt */
    .gs, .highlight .gs {
        color: #f6f3e8 !important;
    } /* Generic.Strong */
    .gu, .highlight .gu {
        color: #f6f3e8 !important;
        font-weight: bold !important;
    } /* Generic.Subheading */
    .gt, .highlight .gt {
        color: #f6f3e8 !important;
    } /* Generic.Traceback */
    .kc, .highlight .kc {
        color: #8ac6f2 !important;
    } /* Keyword.Constant */
    .kd, .highlight .kd {
        color: #8ac6f2 !important;
    } /* Keyword.Declaration */
    .kn, .highlight .kn {
        color: #8ac6f2 !important;
    } /* Keyword.Namespace */
    .kp, .highlight .kp {
        color: #8ac6f2 !important;
    } /* Keyword.Pseudo */
    .kr, .highlight .kr {
        color: #8ac6f2 !important;
    } /* Keyword.Reserved */
    .kt, .highlight .kt {
        color: #cae682 !important;
    } /* Keyword.Type */
    .ld, .highlight .ld {
        color: #f6f3e8 !important;
    } /* Literal.Date */
    .m, .highlight .m {
        color: #e5786d !important;
    } /* Literal.Number */
    .s, .highlight .s {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String */
    .na, .highlight .na {
        color: #cae682 !important;
    } /* Name.Attribute */
    .nb, .highlight .nb {
        color: #f6f3e8 !important;
    } /* Name.Builtin */
    .nc, .highlight .nc {
        color: #f6f3e8 !important;
    } /* Name.Class */
    .no, .highlight .no {
        color: #e5786d !important;
    } /* Name.Constant */
    .nd, .highlight .nd {
        color: #ff0080 !important;
    } /* Name.Decorator */
    .ni, .highlight .ni {
        color: #e7f6da !important;
    } /* Name.Entity */
    .ne, .highlight .ne {
        color: #f6f3e8 !important;
    } /* Name.Exception */
    .nf, .highlight .nf {
        color: #cae682 !important;
    } /* Name.Function */
    .nl, .highlight .nl {
        color: #f6f3e8 !important;
    } /* Name.Label */
    .nn, .highlight .nn {
        color: #f6f3e8 !important;
    } /* Name.Namespace */
    .nx, .highlight .nx {
        color: #f6f3e8 !important;
    } /* Name.Other */
    .py, .highlight .py {
        color: #f6f3e8 !important;
    } /* Name.Property */
    .nt, .highlight .nt {
        color: #8ac6f2 !important;
    } /* Name.Tag */
    .nv, .highlight .nv {
        color: #cae682 !important;
    } /* Name.Variable */
    .ow, .highlight .ow {
        color: #f6f3e8 !important;
    } /* Operator.Word */
    .w, .highlight .w {
        color: #f6f3e8 !important;
    } /* Text.Whitespace */
    .mf, .highlight .mf {
        color: #e5786d !important;
    } /* Literal.Number.Float */
    .mh, .highlight .mh {
        color: #e5786d !important;
    } /* Literal.Number.Hex */
    .mi, .highlight .mi {
        color: #e5786d !important;
    } /* Literal.Number.Integer */
    .mo, .highlight .mo {
        color: #e5786d !important;
    } /* Literal.Number.Oct */
    .sb, .highlight .sb {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Backtick */
    .sc, .highlight .sc {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Char */
    .sd, .highlight .sd {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Doc */
    .s2, .highlight .s2 {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Double */
    .se, .highlight .se {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Escape */
    .sh, .highlight .sh {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Heredoc */
    .si, .highlight .si {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Interpol */
    .sx, .highlight .sx {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Other */
    .sr, .highlight .sr {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Regex */
    .s1, .highlight .s1 {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Single */
    .ss, .highlight .ss {
        color: #95e454 !important;
        font-style: italic !important;
    } /* Literal.String.Symbol */
    .bp, .highlight .bp {
        color: #f6f3e8 !important;
    } /* Name.Builtin.Pseudo */
    .vc, .highlight .vc {
        color: #cae682 !important;
    } /* Name.Variable.Class */
    .vg, .highlight .vg {
        color: #cae682 !important;
    } /* Name.Variable.Global */
    .vi, .highlight .vi {
        color: #cae682 !important;
    } /* Name.Variable.Instance */
    .il, .highlight .il {
        color: #e5786d !important;
    } /* Literal.Number.Integer.Long */
```

