
+++
date = "2014-04-07"
draft = false
title = "Convert to Android Studio and Gradle today!"
slug = "convert-to-android-studio-and-gradle-today"
tags = ['nononsensenotes', 'android', 'gradle']
banner = ""
aliases = ['/convert-to-android-studio-and-gradle-today/']
+++

Took the plunge and converted NoNonsense Notes from Ant and Eclipse to Gradle and Android Studio. It took some googling and a fair bit of frustration but in the end it was very much worth it.

![Android Studio screenshot](/images/2014/Apr/android_studio_shot1.png)

Eclipse has been broken for me for about 6 months or so. So very little of it's powerful features were available during development. The ant build worked fine but Eclipse uses its own which is mysterious and requires constant refreshes and cleaning. Often in a particular order on each library project which was a dependency.

With gradle on the other hand, if it builds in the console then it will build in Android Studio. Once again I am able to use a debugger!

I can warmly recommend people to take a day and setup some _build.gradle_ files.


