
+++
date = "2016-05-18"
draft = false
title = "Set refresh rate of screen from script"
slug = "set-refresh-rate-of-screen-from-script"
tags = ['linux', 'command line', 'gaming']
banner = "/images/2016/05/Selection_034.png"
aliases = ['/set-refresh-rate-of-screen-from-script/']
series = ["Linux 100Hz gaming"]
+++

Getting a great new 100 Hz Ultra Wide monitor does not come without its share of tweaking. So it turns out that the refresh you set on your monitor in Nvidia settings (as explained in a [previous post](https://cowboyprogrammer.org/nvidia-gsync-on-linux/) does not apply to all the display ports. They apparently count as different screens with different settings or something.

So, here's a handy script which you can add to your window manager's autostart applications to set the refresh rate and resolution of your screen, regardless of which actual port you use:


```bash
#!/bin/bash -eu
RES="3440x1440"
RR="100"

# Do for every output, so that it doesn't matter where you plug in
# your monitor.
for output in $(xrandr | grep "DP-" | sed -e "s/\(DP-.\).*/\1/"); do
  echo "Trying to set mode on $output"
  if xrandr --output "$output" --mode "$RES" -r "$RR"; then
    echo "Success: $RES $RR Hz set on $output"
  fi
done
```

It iterates over all the display ports on your graphics card, so it doesn't matter where you plug your monitor in.

In XFCE, you'd add this script to *Application Autostart*:

![XFCE Application Autostart](/images/2016/05/Session-and-Startup_033.png)
