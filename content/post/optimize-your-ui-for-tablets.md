
+++
date = "2014-04-07"
draft = true
title = "So many ways to fail with UI design"
slug = "optimize-your-ui-for-tablets"
tags = ['android']
banner = ""
aliases = ['/optimize-your-ui-for-tablets/']
+++

## Draft 2

OK, it is finally time to rant about this "Tablet UI issue" and all the ways some developers seem to go out of their way to fuck with your usage of

## Draft 1

It's 2014 and we still have to put up with apps that don't scale on big screen sizes. All you lazy developers out there, it's time to **listen up**.

## If you're lazy or just don't have any better ideas

Start with your basic phone UI, just a list for example. But design the UI with a huge 10 inch screen in mind. Instead of scaling the list to the entire width, you set a maximum DP-size and let the rest be margins. It's not super pretty but at least readability doesn't suffer.

Zite is a clear example of an app which doesn't even try to scale to bigger screens:

![Zite is an example of this shit](http://)

If we look at the code, a simple fullscreen list looks something like this:

![no scaling layout](/images/2014/May/NoScaling-1.png)

Changing a single line at least makes your app readable on larger screens:

![max width layout](/images/2014/May/ScaleMaxWidth.png)

## Design your app for tablets first, then scale down

It's much easier to scale the UI down to smaller screens because essentially you display a single component there and it always looks good because there is no space for stupid scaling or huge margins. If you design your tablet UI using fragments, it is not even technically difficult to make a smaller version of the UI.

## Different types of UIs

What works depends entirely on what information you want to display.

### The grid

Works really well if you content involves images. Works less well for pure text. Doesn't work at all if your content is best thought of as a list.

### The list and details view

The new project wizard in Eclipse can generate this for you. It works really well for certain types of data. E-mail is a prime example. Works less well when the details are short and won't fill all the available space. NoNonsenseNotes suffers from this. Notes and tasks are typically quite short, so there is so much wasted space on the detail side.

### The dashboard

Don't confuse it with the old pattern of top level icon used for navigation (we have the sliding side menu for that). This is a good way to display data that is list structured and typically short. The key is that we are able to display many different views of the data at once.

### The floating window

Say you really can't fill all the space that a two pane layout gives you. You're really out of ideas but don't want to just scale your small screen UI to the bigger screen. One idea is to just don't. Let it remain as the same smaller UI, and instead display it as a floating window. Link Bubble is one app which has a variation of this.

