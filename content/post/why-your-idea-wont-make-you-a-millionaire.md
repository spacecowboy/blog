
+++
date = "2014-04-28"
draft = true
title = "Why your idea won't make you a millionaire"
slug = "why-your-idea-wont-make-you-a-millionaire"
tags = []
banner = ""
aliases = ['/why-your-idea-wont-make-you-a-millionaire/']
+++

* Silicon Valley episode 3 - dude with idea for notepad but only for writing down where you parked. It's a bad idea, bad execution.
* But it would have led to the conclusion that he wanted a good notepad app. And he's not satisfied with current offerings.
* So new idea: a great notepad app.
* What's wrong with existing bajillion notepad apps? What can and needs to be improved.
* Simple ideas CAN make money
* But there are often many versions of that idea (every community ever converged to facebook)
* It's random chance, and marketing, that makes the idea take over. You need to get over the hump, then people will buy it because everybody else is.

