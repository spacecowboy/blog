
+++
date = "2014-04-26"
draft = false
title = "Are iPads actually a step back?"
slug = "are-ipads-retarding-us"
tags = ['linux', 'windows', 'ios', 'os x', 'computers', 'gaming']
banner = ""
aliases = ['/are-ipads-retarding-us/']
+++

Think what you will of the iPad, but it has been a huge success for Apple and people love it. It's one of the few products that appealed (past-tense) to both the geeks and _[hoi polloi](http://en.wikipedia.org/wiki/Hoi_polloi)_.

I remember watching the keynote where the iPhone was introduced and immediately I thought _that's the pad from Star-Trek TNG!_ I **had** to have it.
Apple's initial carrier exclusivity deals meant I had to wait for the iPhone 3g. Not only that, but because they partnered with a company I am sworned to destroy, I payed a guy in Italy to buy one unlocked and ship it to me for 7500SEK. Funny thing is that at the time I was a developer at Sony Ericsson, who did not think kindly of Apple entering their mobile domain. I got a lot of weird looks a work...

Then came the iPad. Again I'm thinking _holy shit that's awesome._ At this point I had upgraded to an Android device (an HTC Legend) and had come to the conclusion that Android was far more interesting as a platform because of Apple's restrictions on what apps can do. The customizability and capabilities on Android were far greater and as a developer, you appreciate that. However, _there were no Android tablets._ And there wouldn't be for a long time.

I kept my cool though and managed to hold on to my money until the iPad 2 was released. I left early from work and lined up with other enthusiasts at the electronics store. At the time most people had no idea what they were going to do with it, me included, but I __had__ to have it. I think my extended family clearly demonstrates how successful a product the iPad was. That same year I saw 3 iPads being gifted away (3 in a group of around 9 people!). By the next year, 3 more iPads were acquired. Everyone had to have one. It was one of those cases where you don't get it until you see it for yourself.

## From revolutionary to evolutionary

It is both a sign of how good the original product was and how little has changed that I never felt a reason to upgrade from the iPad 2.

- The battery life was fantastic.
- The screen size just right.
- The resolution was good enough.
- The speed was fine (until recently).

Hardware-wise, it was feature complete. The rest could be fixed in software. They never did though. The problem is iOS. Just as I abandoned the iPhone for Android, I now abandoned the iPad for a Nexus 7. There was so much potential being held back by the limitations of iOS.
[Stratechery](http://stratechery.com/2014/dont-give-ipad/) explains some of my frustrations well. He means it as a defense in iOS's favor though. But there is actually more to it than the limitations of iOS. Something inherent in the touch screen and the current mobile paradigm.

## Limitations of the touch screen

I was playing [R-Type 2](https://play.google.com/store/apps/details?id=com.dotemu.rtype2) on my Nexus 10 and kept dying on the boss in the second level. And I realized that while I might get lucky and finish the level, I would never be able to play the game well due to the touch screen.

![R-Type 2 second level boss](/images/2014/Apr/rtype2-boss.png)

See, R-Type is a classic side-scrolling _shoot-em-up_. You pilot a spaceship and have to avoid enemy fire, hordes of enemies, and not crash into the roof or ceiling. It is a game based entirely on mastering the controls. You can see a good example of what I mean in this clip of a similar game called _Gradius_ for the NES.

<iframe width="420" height="315" src="//www.youtube.com/embed/3PMpbPYB0iY" frameborder="0" allowfullscreen></iframe>

The problem I was having was that I kept crashing into the floor as I tried to manouever around the boss. Having played for and hour or two (and still being stuck on level 2!) I came to realize that it wasn't I that sucked, it was the controls. I had reached the limit of what was possible (precision-wise) with a touch screen.

### Noobs forever

And this is where the back-stepping begins. Growing up with NES, SNES, and a PC, I remember many older relatives noting the dexterity and precision in the thumbs of kids due to all the gaming. Video games required:

- hand-eye-coordination
- hand dexterity
- concentration

To beat these games you needed _mastery_ and _focus_. Not only was mastery required, it was the _reward_. The games suitable for touch screens can require neither. So tablet games will remain at a level no more advanced than snake or scrabble. (As a side note, what really can work is turn-based strategy games.)

### No such thing as a touch typist
Just as serious gaming becomes impossible due to the touch interface, serious productivity suffers from the same limitations. It's funny to see things like Microsoft Office being released for the iPad because it's impossible to work with. Serious productivity requires the efficient inputting of language, be it English or Python. The touch keyboard is unable to let you do that. There is no such thing as a touch typist. On a tablet, everyone goes back to tapping with two fingers. There is nothing to master here (due to the lack of feedback) and so everyone will remain as noobs forever.

## The dark age begins
Maybe you're thinking to yourself:

>_so what if a touch screen isn't ideal for everything, no input device is!_ 

If you are, then I agree. Nothing can be great at everything. You use the right tool for the right job. The problem is the tremendous success of the tablet. This is where I think the geeks have a different view of where we are headed.

Geeks see the benefits of the touch screen. Its strengths, but also its weaknesses. They use it when it's convenient. For more serious work, they move to their workstation, with keyboard and screen.

Non-geeks see the tablet as _"the future"_. They never liked their PC to begin with. It was just something they were forced to acquire to be able to pay their bills. They see the tablet as liberating. Geeks see the tablet as confining.

The success of the tablet amongst geeks and non-geeks combined means companies are scrambling to push everything into tablet interfaces. Apple is clearly moving towards iOS as OSX is evolving. Microsoft has already gone too far:

<iframe width="560" height="315" src="//www.youtube.com/embed/WTYet-qf1jo" frameborder="0" allowfullscreen></iframe>
    
But it's not just the tablet interface. It's the whole mobile paradigm that is spreading. With it comes the _app stores_, where every app is pre-approved by the benevolent corporation that owns your ~~soul~~ apps and music. The corporation reserves the right to remove any app or in-app purchase it deems unworthy of your attention. [Amazon did it](http://www.nytimes.com/2009/07/18/technology/companies/18amazon.html?_r=0), [Apple does it all the time](http://www.pcworld.com/article/2095060/apple-removes-blockchain-last-bitcoin-wallet-app-from-mobile-store.html), [and same for Microsoft](http://www.wired.co.uk/news/archive/2014-01/20/microsoft-removes-tor).

I would say that Linux is the only alternative going forward, but then again, [Canonical is showing that not even Linux is safe](https://www.gnu.org/philosophy/ubuntu-spyware.html).

