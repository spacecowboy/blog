+++
banner = "/images/2021/06/animated-webp-supported.webp"
categories = []
date = "2021-06-09T21:43:00+02:00"
description = ""
draft = false
images = []
menu = ""
series = ["Rewriting Feeder in Compose"]
tags = ["feeder", "android"]
title = "The biggest update to Feeder so far"

+++

Just a placeholder so far. Needed a known blog to test a few things with.

- [Images](#images)
  * [Red dot](#image-red-dot)
- [Text formatting](#text-formatting)
- [Headers](#header-1)
- [Lists](#lists)
  * [Bullet 1](#bullet-1)
  * [Bullet 2](#bullet-2)
  * [Bullet 3](#bullet-3)
- [Videos](#videos)
- [Audio](#audio)

## Images

![Animated Webp image](/images/2021/06/animated-webp-supported.webp)

![Animated Gif](/images/2021/06/rotating_earth.gif)

And at long last animated in the reader itself!

![Animated reader](/images/2021/06/reader_animated.gif)

Base64 encoded inline data image

<img id="image-red-dot" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==" alt="Red dot">

## Text formatting

A [link](https://gitlab.com/spacecowboy/Feeder/-/merge_requests/318) to Gitlab.

Some `inline code formatting`.

And then

```
A code block
with some lines
of code should be scrollable if
one very very long line with many  sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
```

A table!

<table>
<caption>
<p>Table 1.
<p>This table demonstrates the table rendering capabilities of Feeder's Reader view. This caption
is by the spec allowed to contain most objects, except other tables. See
<a href="https://www.w3.org/TR/2014/REC-html5-20141028/dom.html#flow-content-1">flow content</a>.
</caption>
<thead>
<tr>
<th>Name
<th>Number
<th>Money
<tr>
<th>First and Last name
<th>What number human are you?
<th>How much money have you collected?
<tfoot>
<tr>
<th>No Comment
<th>Early!
<th>Sad
<tbody>
<tr>
<td>Bob
<td>66
<td>$3
<tr>
<td>Alice
<td>999
<td>$999999
<tr>
<td>:O
<td colspan="2">OMG Col span 2
<tr>
<td colspan="3">WHAAAT. Triple span?!
<tr>
<td colspan="0">Firefox special zero span means to the end!
</table>

And this is a table with an image in it

<table>
<tbody>
<tr>
<td>
<img src="https://cowboyprogrammer.org/images/Ardebian_logo_512_0.png" alt="Debian logo">
</td>
</tr>
<tr>
<td>
Should be a debian logo above
</td>
</tr>
</tbody>
</table>

And this is a link with an image inside

<a href="https://cowboyprogrammer.org/2016/08/zopfli_all_the_things/">
<img src="https://cowboyprogrammer.org/images/2017/10/zopfli_all_the_things_32.png" alt="A meme">
</a>

Here is a blockquote with a nested quote in it:

> Once upon a time
>
> A dev coded compose
> it was written:
>
>> @Composable
>> fun FunctionFuns()
>
> And there was code

Here comes some headers

# Header 1

## Header 2

### Header 3

#### Header 4

##### Header 5

###### Header 6

## Lists

Here are some lists

* <div id="bullet-1">Bullet</div>
* <div id="bullet-2">Point</div>
* <div id="bullet-3">List</div>

and

1. Numbered
2. List
3. Here

## Videos

Here's an embedded youtube video

<iframe width="560" height="315" src="https://www.youtube.com/embed/1OfxlSG6q5Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here's an HTML5 video

<video width="320" height="240" controls>
  <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
  <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
Your browser does not support the video tag.
</video>

## Audio

<audio controls>
  <source src="/audio/mp3-example-file-download-1min.mp3" type="audio/mpeg">
  <source src="/audio/ogg-example-file-download-1min.ogg" type="audio/ogg">
  Your browser does not support HTML5 audio playback.
</audio>
