
+++
date = "2015-08-25"
draft = true
title = "Blocking ads and spyware with pfSense"
slug = "blocking-ads-and-spyware-with-pfsense"
tags = ['tutorials', 'pfsense', 'sysadmin']
banner = ""
aliases = ['/blocking-ads-and-spyware-with-pfsense/']
+++

## Intro: pfSense with squidGuard and automatically updating hpHosts file

The nice thing about having an actual computer as your router/firewall is that you can
do interesting things with it. For example blocking known malware/spying domains.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q0JFfpG4BWI" frameborder="0" allowfullscreen></iframe>

## Choose a blocklist

There are many blocklists available out there so just Google around a bit to explore. Personally however, I like [hpHosts][hphosts]. It has worked well for me on my local machine and I appreciate their policy of blocking only malicious sites as compared to many blocklists which also support censoring useful information like sexual education, webmail sites, etc.

Only problem with hpHosts is that only a pure hosts file is provided, which is not compatible with squidGuard. That's why I wrote this handy script to download and reformat it:

```bash
#!/bin/bash

TEMPDIR=/tmp/hphostspfsense

# Move to temporary directory
mkdir -p $TEMPDIR
rm -rf $TEMPDIR/*

cd $TEMPDIR

# Download latest hp-hosts file
wget http://hosts-file.net/download/hosts.zip

# Extract
unzip hosts.zip

# Make category folder
mkdir hphosts

# Remove lines which do not matter
# sed '0,/BAD HOSTS BEGIN HERE/d'
cat hosts.txt | sed '/localhost/d' > hphosts/domains
cat hphosts/domains | sed '/^#/d' > hphosts/domains.temp

# Format it for pfsense
cat hphosts/domains.temp | sed -r 's/127.0.0.1\s+//' > hphosts/domains

# Remove temp file
rm hphosts/domains.temp

# Make package
tar -caf pfsensehphosts.tar.gz hphosts

# Copy to destination, change destination to whatever you need
cp pfsensehphosts.tar.gz /var/www/publicwebsite/
```

The resulting archive is compatible with squidGuard. My script runs in a cronjob once a day and puts the file here: [pfsensehphosts.tar.gz](http://publicfiles.cowboyprogrammer.org/pfsensehphosts.tar.gz). Feel free to use it in your pfSense configuration, though I make no guarantees about its availability.


## Configure squidGuard

Listed under Services->Proxy filter

![](/images/2015/08/2015-08-25-175533_124x311_scrot.png)

### Tab: General settings

Configure the blocklist.

![Input the download address](/images/2015/08/2015-08-25-175404_937x352_scrot-1.png)

### Tab: Blacklist

Download it.

![Download the blocklist](/images/2015/08/2015-08-25-175116_779x445_scrot.png)

### Tab: Common ACL
Set the hphosts category as deny.

![Set deny on the hphosts category](/images/2015/08/2015-08-25-175239_953x653_scrot.png)

### Tab: General settings

Remember to click __apply__!

![very important apply button](/images/2015/08/2015-08-25-174735_367x115_scrot.png)

And as a final step, make sure squid is running and transparent proxy is enabled.

## Automatically download updated blocklist

Move script from tmp to root

    cp /tmp/squidGuard_blacklist_update.sh /root/

Add crontab entry

    0 4 * * * /root/squidGuard_blacklist_update.sh

## Hardware I happen to use

[hphosts]: http://www.hosts-file.net/  "hpHosts"
