
+++
date = "2014-05-23"
draft = true
title = "My thoughts on blogging"
slug = "my-thoughts-on-blogging"
tags = []
banner = ""
aliases = ['/my-thoughts-on-blogging/']
+++

* I take my time on posts
* I have several drafts in parallel
* I want to publish things that have some message
* Somethings I just want to exist on the web so I can find it myself later

