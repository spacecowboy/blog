+++
date = "2016-10-19T00:00:00+02:00"
draft = false
title = "Don't start service on installation of Debian package"
slug = "dont-start-service-on-install-of-debian-package"
tags = ['linux', 'debian', 'ubuntu', 'systemd']
banner = "/images/Ardebian_logo_512_0.png"
aliases = ['/dont-start-service-on-install-of-debian-package/']
+++

A clear difference between Debian/Ubuntu and for example Red
Hat/Fedora is that packages which include system services will enable
and start those services at install time in Debian/Ubuntu whereas they
will not start automatically in Red Hat/Fedora.

Sometimes it would be very convenient if the service would *not* start
automatically, for example if you need to configure the service before
starting it for the first time.

To prevent the automatic start of system services at install time in
Debian, just set the `RUNLEVEL` environment variable like so:

```
RUNLEVEL=1 apt install -y PKG_NAME
```

Then you are free to configure your system before you start the
service for real:

```
systemctl enable PKG_NAME
systemctl start PKG_NAME
```
