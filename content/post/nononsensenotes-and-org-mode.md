
+++
date = "2014-04-07"
draft = false
title = "NoNonsenseNotes and org-mode"
slug = "nononsensenotes-and-org-mode"
tags = ['nononsensenotes', 'org', 'android']
banner = ""
aliases = ['/nononsensenotes-and-org-mode/']
+++

One of the oldest feature requests for my notes app that still is not implemented is
[plain file storage](https://github.com/spacecowboy/NotePad/issues/220). It's not entirely trivial to keep the files and the database in sync while also avoiding conflicts with Google Tasks. And because I never had any need of it myself, it always got a very low priority.

However, after a user [introduced me to org-mode](https://github.com/spacecowboy/NotePad/issues/254) I have felt more and more need of this. Since I'm an emacs user, [orgmode](http://orgmode.org/) fits me nicely (I'm writing this blog post in org-mode btw).

## But what IS org-mode?

You could say that org-mode is a plugin for emacs for doing todo lists, reports and just about any document with a structure (or not). But org-mode is also a specification for a plaintext format.

At a glance it appears similar to [markdown](http://daringfireball.net/projects/markdown/) formatting, since you can make text **bold**, *italicized* or <span class="underline">underlined</span>. But doing markup for your text is all markdown does. org-mode goes further. Much further&#x2026;

org-mode additionally specifies that entries in a document can be labeled as **TODO** or **DONE** (you can specify more yourself). They can have dates, scheduling and deadlines. And items can be tagged. These are all the building blocks of a todo-database making the org-mode format highly appropriate for plain text exporting/importing.
 
I have already implemented an org-mode exporter in the app. What's missing is an importer. That I am developing in parallel with my next project.

## NoNonsenseOrg (name not final)

org-mode offers so many more features than Google Tasks. It's also free and human-readable (no lockin). NoNonsenseNotes have always been between being an app for notes and an app for todo lists. My initial
plan was to just do simple notes, like the built-in notes app in iOS. GTasks was just a convienent way to do cross-device synchronization. But it lead me to implement todo-centric features and thus the app was no longer a simple notes app.

org-mode does not make any requirements. If you just want notes, that's all it is. If you want todo lists, it can give you that too. So that solves the identity problem. Looking at the todo-features however, it's clear that it also offers way more functionality than GTasks:

-   user-specified todo states,
    like: **TODO**, **NEXT**, **PENDING**, **DONE**, **CANCELLED**
-   tags
-   multiple deadlines (due-dates)
-   a task can be scheduled for a duration: from 8 to 9 on tuesday
-   text *can* <span class="underline">be</span> **highlighted**

So what I'm gonna do is implement an app specifically for org-mode. The parser (almost complete) will be used for the export/import feature in NoNonsenseNotes. There is of course already an Android app for org-mode called [mobileorg](https://github.com/matburt/mobileorg-android). Which was actually started even before NoNonsenseNotes.

So why do I want to make NoNonsenseOrg? I made NoNonsenseNotes originally because no existing notes app followed the style and guidelines in Android 4.0 (Ice Cream Sandwhich). The same is true for org-mode today.

It's not my intention to be critical here. The amount of change in Android has been dramatic the last years. It was only in the second half of 2013 in which we could start talking about a mature platform and style. Rewriting UIs is a huge amount of work. Which is why I'm initially starting from scratch to get some things right which are still wrong in NoNonsenseNotes.

One thing that I now feel is wrong is for example the license. NoNonsenseNotes is released under the [Apache v2 license](http://www.apache.org/licenses/LICENSE-2.0.html) which basically says "take the code and do what you wish" in my interpretation. NoNonsenseOrg will be released under [GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html). An important part of this license says

> One of the fundamental requirements of the GPL is that when you
> distribute object code to users, you must also provide them with a way
> to get the source.

So anyone wishing to use the code in their own projects may do so, but they must also publish their own modifications.

Updates will be posted as work happens. Feel free to suggest features and such in the [forum](http://forum.cowboyprogrammer.org).

