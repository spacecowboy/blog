
+++
date = "2014-04-07"
draft = false
title = "How to convert your project to Gradle"
slug = "how-to-convert-your-project-to-gradle"
tags = ['android']
banner = ""
aliases = ['/how-to-convert-your-project-to-gradle/']
+++

To follow up my last post, here are some practical advice if you are interested in moving to Gradle and Android Studio. Once you've set up a working gradle build, all you have to do is select "Import Project" in Android Studio and it will sort everything out. Compare that to importing projects in Eclipse which is a pure nightmare.

## Root directory and settings file

This is important if your project is split into several libraries. Your app probably just has one directory. Put that directory inside another directory. For example, if you have a directory MyApp, then create a new directory so you have MyProject/MyApp for example. The root of your project is now MyProject/.

In the root of your project, add this build.gradle file:

```groovy
// Top-level build file where you can add configuration options common
// to all sub-projects/modules.
    
buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath 'com.android.tools.build:gradle:0.9.+'
  }
}
    
allprojects {
  repositories {
    mavenCentral()
  }
}
    
task wrapper(type: Wrapper) {
  gradleVersion = '1.10'
}
```

You also need a settings.gradle file (probably). It contains the paths/names of any sub projects, libraries etc. If your root directory is MyProject/, and your app is in MyProject/MyApp/, this works:

```groovy
include ':MyApp'
```

If you are also using any library projects, add them to this file as well:

```groovy
include ':MyApp', ':SomeLibrary', ':subdir:AnotherLibrary'
```

Assuming you have libraries in MyProject/SomeLibrary and MyProject/subdir/AnotherLibrary. The libraries must also have valid gradle files. But adding that is easy, just read on.

## Script for existing projects

If you have an existing project using Eclipse/Ant, you likely have the following folders:
-   src
-   res
-   lib

You don't have to change your folder structure to use gradle. If you don't reference any library projects and only use jar-files in the lib-folder, the following script should be all you need:

```groovy
buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath 'com.android.tools.build:gradle:0.9.+'
  }
}
    
apply plugin: 'android'
    
repositories {
  mavenCentral()
  mavenLocal()
}
    
dependencies {
  compile fileTree(dir: 'libs', include: '*.jar')
}
    
android {
  compileSdkVersion 19
  buildToolsVersion "19.0.3"
    
  sourceSets {
    debug.setRoot('build-types/debug')
    release.setRoot('build-types/release')
    
    main {
      manifest.srcFile 'AndroidManifest.xml'
      java.srcDirs = ['src']
      resources.srcDirs = ['src']
      aidl.srcDirs = ['src']
      renderscript.srcDirs = ['src']
      res.srcDirs = ['res']
      assets.srcDirs = ['assets']
    }
  }
}
```

## Library projects

Working on a library project? Then you need to change one line:

```groovy
// Change this line:
// apply plugin: 'android'
// to this instead:
apply plugin: 'android-library'
```

## Replace some libs with maven dependencies

If you are using the support-library, you probably have android-support-v4.jar in your lib directory. Using gradle, you can delete that and let gradle sort out the dependency for you. Delete the jar-file and add a compile dependency to your build.gradle file:

```groovy
dependencies {
  compile fileTree(dir: 'libs', include: '*.jar')
  compile 'com.android.support:support-v4:19.0.1+'
}
```

Many libraries are uploaded to MavenCentral. If they are, all you need is one such line in your build.gradle and you're ready to use them.

Another example for Google Play Services and Joda Time:

```groovy
dependencies {
  compile fileTree(dir: 'libs', include: '*.jar')
  compile 'com.android.support:support-v4:19.0.1+'
  compile 'com.google.android.gms:play-services:4.2.42+'
  compile 'joda-time:joda-time:2.3'
}
```

Note that you can't keep the jar-file if you add the maven dependency.

## Referencing library projects

So how do you reference the library projects you set up in settings.gradle earlier? Simply by adding some lines to dependencies:

```groovy
dependencies {
  // Your standard jar-files
  compile fileTree(dir: 'libs', include: '*.jar')
  // Example of MavenCentral dependencies
  compile 'com.android.support:support-v4:19.0.1+'
  compile 'com.google.android.gms:play-services:4.2.42+'
  compile 'joda-time:joda-time:2.3'
    
  // Library projects defined in settings.gradle
  compile project(':SomeLibrary')
  compile project(':subdir:AnotherLibrary')
}
```

## Syntax highlighting in Emacs

Gradle uses the Groovy language, so adding this line to your .emacs gives you syntax highlighting and so on:

    ;; Gradle files should use Groovy Mode
    (add-to-list 'auto-mode-alist '("\\.gradle\\'" . groovy-mode))


