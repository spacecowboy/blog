
+++
date = "2014-04-27"
draft = true
title = "The power of Git"
slug = "the-power-of-git"
tags = ['git', 'emacs', 'command line']
banner = ""
aliases = ['/the-power-of-git/']
+++

* Version control and git in particular, rising in popularity.
* Plenty of beginner stuff
* Time for some good intermediate stuff

If you are a beginner programmer, you probably don't see the need for version control yet. After all, your changes are all "small", and "obvious". If you have any questions you can _just read the code_, because _"the code is the documentation"_. If you think this any of these are true, let me be the first to burst your bubble:

* Your "fixes" are **not** obvious
* Your code is **unreadable** for everyone, including you 2 years from now
* Your code makes no sense and your documentation strings suck

You are also probably thinking that version control might be great for teams, but not so when it's just you working on the project. After all, you can just keep everything in your head, right? You are right that version control really shines in collaborations, but you're also wrong when you think you are working alone. You _are_ working in a team. Your "one man" team consists of three people: your past self, your present self, and your future self. They are as different from one another when it comes to code understanding as three complete strangers. And if past you did not use version control, the future you(s) will hate you for it.

> Your "one man" team consists of: your past self, your present self, and your future self.

## How to talk to yourself, 2 years from now

I will assume you are using git, because that's what I am most familiar with, but the message is that same regardless.

Whenever you make a commit, git will ask you to write a commit message. Typing just

    git commit
    
will open your editor of choice. If you have just a short message, then you can do

    git commit -m "Fixing bug #99"
    

### Write proper commit messages

Please don't write silly one-liners like:

    git commit -m "Just testing something..."
    git commit -m "New version"
    git commit -m "Fix bug"
    git commit -m "bah"
    git commit -m "..."
    
The reason is because that message doesn't explain anything. Doing a diff will show _what_ has changed. The commit message is supposed to explain _why_ the change was made. A real commit message looks something like:

>Fixes the scrolling bug in the main window
>
>When scrolling to the bottom of the list, the list
>would completely freak out and delete everything.
>This fix makes sure that the items are correctly
>retained.

This clearly explains what the intent of the fix is. In essence, the only one-line commit messages that I approve are:

    git commit -m "Fix spelling errors"
    git commit -m "Bump version"
    
### Make many small commits instead of few larger ones

It is easy to forget to commit things when you're working on new functionality. Before you know it, you've changed 10 files and can't say much about this other than "Implements feature X". But even if you forget to commit stuff during, there is no reason to commit everything in a single go. You can stage individual files, or even individual pieces of files, into smaller commits when you're done.



# Global files

- global config
- global ignore

# Aliases
* Take mine as examples, like git purr, lg and st.

# Branches
Branch like there's no tomorrow! But remember to delete them too.

# Rebase
- Why is a linear history good?
- Write it for yourself, 2 years from now.

# Cherry pick
* add a remote and cherry pick their awesome stuff
* keep your commits small!
* Example of where you are benefitting from indirect cooperation.

# Hooks
* Send email about changes made
* Run unit tests (your own continuous integration!)
* Check copyright headers
* Check commit messages

# Blame
- true reason why good commit messages are needed
- emacs blame mode

# bisect
- bug finding power

