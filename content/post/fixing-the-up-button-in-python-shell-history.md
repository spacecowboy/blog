
+++
date = "2016-04-02"
draft = false
title = "Fixing the up button in Python shell history"
slug = "fixing-the-up-button-in-python-shell-history"
tags = ['python', 'command line', 'programming']
banner = "/images/2016/04/Selection_021-1.png"
aliases = ['/fixing-the-up-button-in-python-shell-history/']
+++

In case your python/ipython shell doesn't have a working history, e.g. pressing &#8593; only prints some nonsensical `^[[A`, then you are missing either the `readline` or `ncurses` library.

![Python shell where up doesn't work](/images/2016/04/Selection_021.png)

Ipython is more descriptive that something is wrong, but if you're in the habit of mostly using python as a quick calculator, you might never notice:

![iPython shell where up doesn't work](/images/2016/04/Selection_022.png)

If you're using [Miniconda](http://conda.pydata.org/miniconda.html) then just do:

    conda install ncurses readline

And &#8593; should work:

![iPython with working up](/images/2016/04/Selection_023.png)

