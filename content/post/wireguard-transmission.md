+++
banner = ""
categories = []
date = "2019-09-01T23:21:00+02:00"
description = "How to setup the tunnel, the routes, and transmission."
images = []
menu = ""
tags = ["wireguard"]
title = "How to setup Transmission-daemon over a Wireguard VPN"

+++

Quick post to immortilize the configuration to get transmission-daemon working with a wireguard tunnel.

If you don't have a wireguard tunnel, head to <https://mullvad.net/en/> and get one.

## Transmission config

First, the transmission config is really simple:

```
#/etc/transmission-daemon/settings.json
{
  [...]

  "bind-address-ipv4": "X.X.X.X",
  "bind-address-ipv6": "xxxx:xxxx:xxxx:xxxx::xxxx",
  "peer-port": 24328,
  "rpc-bind-address": "0.0.0.0",

  [...]
}
```

I also run the daemon using the following service for good measure:

```
# /etc/systemd/system/transmission-daemon.service
[Unit]
Description=Transmission BitTorrent Daemon Under VPN
After=network-online.target
After=wg-quick@wgtorrents.service
Requires=wg-quick@wgtorrents.service

[Service]
User=debian-transmission
ExecStart=/usr/bin/transmission-daemon -f --log-error --bind-address-ipv4 X.X.X.X --bind-address-ipv6 xxxx:xxxx:xxxx:xxxx::xxxx --rpc-bind-address 0.0.0.0

[Install]
WantedBy=multi-user.target

```

## Wireguard config

All the magic happens in the PostUp rule where a routing rule is added for any traffic originating from the wireguard IP addresses.

```
#/etc/wireguard/wgtorrents.conf
[Interface]
PrivateKey=
Address=X.X.X.X/32,xxxx:xxxx:xxxx:xxxx::xxxx/128
# Inhibit default table creation
Table=off
# But do create a default route for the specific ip addresses
PostUp = systemd-resolve -i %i --set-dns=193.138.218.74 --set-domain=~.; ip rule add from X.X.X.X table 42; ip route add default dev %i table 42; ip -6 rule add from xxxx:xxxx:xxxx:xxxx::xxxx table 42
PostDown = ip rule del from X.X.X.X table 42; ip -6 rule del from xxxx:xxxx:xxxx:xxxx::xxxx table 42

[Peer]
PersistentKeepalive=25
PublicKey=m4jnogFbACz7LByjo++8z5+1WV0BuR1T7E1OWA+n8h0=
Endpoint=se4-wireguard.mullvad.net:51820
AllowedIPs=0.0.0.0/0,::/0
```

Enable it all by doing

```
systemctl enable --now wg-quick@wgtorrents.timer
systemctl enable --now transmission-daemon.service
```
