
+++
date = "2014-04-25"
draft = true
title = "Making that app"
slug = "making-that-app-getting-started-with-open-source"
tags = []
banner = ""
aliases = ['/making-that-app-getting-started-with-open-source/']
+++

# Make it in steps
You want to get some quick rewards to keep yourself motivated. Have a main feature that's fairly quick and easy to get working. Then start working on the UI and other fluff.

# Use it!
Once you have that basic thing up and running, use it! Only make something you actually want to use or you'll never finish it.

# Make it pretty
Polish that UI!

# Extend it
Add network sync, offline support...

