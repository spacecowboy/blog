+++
banner = "/images/2018/03/5y_avg_rates.png"
categories = []
date = "2018-03-05T23:00:00+02:00"
description = "En jämförelse mellan bunden och rörlig ränta."
images = []
menu = ""
tags = ["lån", "finans", "bolån", "ränta"]
title = "En jämförelse mellan bunden och rörlig ränta"

+++

Eftersom jag kommer bli med lån till sommaren tyckte jag det var tid
att jämföra det här med bunden och rörlig ränta. En bunden ränta känns
trygg: man vet exakt vad man behöver betala varje månad i så och så
många år. Den rörliga räntan är dock generellt lägre och lockar därmed
med mindre räntekostnader. Frågan är, kommer den att förbli lägre över
tid?

Den givna tanken är väl att en rörlig ränta är bäst i tider när räntan
håller på att sjunka, och att bunden ränta är bäst då räntan
ökar. Naivt sätt så inbillar man sig då att man är bunden till en
lägre ränta medan den rörliga går om. Problemet med detta resonemang
är att man inte tar i beaktan hur stor skillnaden är mellan de olika
räntesatserna och hur snabbt räntan förändras.

Men istället för att argumentera över olika idéer, låt oss helt enkelt
jämföra de historiska räntorna och se vad som skulle varit mest
fördelaktigt över olika bindetider.

Datan jag kommer använda kommer ursprungligen från [SwedBank][1] och
all data och kod finns tillgänglig på [GitLab][2]. [Datan][3] innehåller
ränta vid 5 års bindningstid, 2 års bindningstid, samt 3 månaders
bindningstid (även kallad rörlig ränta) för de datum då räntan
förändrades. Första räntesatserna är från 1989-11-01 och sista är från
2018-02-12. Exempel på datan:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>5y</th>
      <th>2y</th>
      <th>3m</th>
    </tr>
    <tr>
      <th>Date</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1989-11-22</th>
      <td>13.50</td>
      <td>13.50</td>
      <td>12.75</td>
    </tr>
    <tr>
      <th>1991-01-14</th>
      <td>14.00</td>
      <td>14.75</td>
      <td>15.25</td>
    </tr>
    <tr>
      <th>1993-01-13</th>
      <td>12.75</td>
      <td>13.00</td>
      <td>13.75</td>
    </tr>
    <tr>
      <th>1994-11-21</th>
      <td>11.75</td>
      <td>11.50</td>
      <td>9.75</td>
    </tr>
    <tr>
      <th>1996-03-12</th>
      <td>9.85</td>
      <td>8.95</td>
      <td>9.10</td>
    </tr>
    <tr>
      <th>2005-09-09</th>
      <td>3.55</td>
      <td>2.97</td>
      <td>3.15</td>
    </tr>
    <tr>
      <th>2005-10-03</th>
      <td>3.69</td>
      <td>3.09</td>
      <td>3.15</td>
    </tr>
    <tr>
      <th>2007-12-21</th>
      <td>5.36</td>
      <td>5.25</td>
      <td>5.15</td>
    </tr>
    <tr>
      <th>2008-01-24</th>
      <td>5.13</td>
      <td>4.94</td>
      <td>5.15</td>
    </tr>
    <tr>
      <th>2009-03-20</th>
      <td>4.26</td>
      <td>2.83</td>
      <td>2.20</td>
    </tr>
  </tbody>
</table>

För att göra beräkningarna lite enklare så antar jag att lån endast
binds första dagen i månaden. Exempel:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>5y</th>
      <th>2y</th>
      <th>3m</th>
    </tr>
    <tr>
      <th>Date</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1990-06-01</th>
      <td>14.50</td>
      <td>14.50</td>
      <td>13.95</td>
    </tr>
    <tr>
      <th>1992-03-01</th>
      <td>12.50</td>
      <td>13.00</td>
      <td>14.75</td>
    </tr>
    <tr>
      <th>1993-06-01</th>
      <td>10.75</td>
      <td>10.50</td>
      <td>11.50</td>
    </tr>
    <tr>
      <th>1998-02-01</th>
      <td>6.70</td>
      <td>6.40</td>
      <td>5.80</td>
    </tr>
    <tr>
      <th>2001-09-01</th>
      <td>6.55</td>
      <td>5.95</td>
      <td>5.90</td>
    </tr>
    <tr>
      <th>2004-11-01</th>
      <td>4.85</td>
      <td>3.90</td>
      <td>3.65</td>
    </tr>
    <tr>
      <th>2009-05-01</th>
      <td>4.15</td>
      <td>2.73</td>
      <td>1.97</td>
    </tr>
    <tr>
      <th>2010-08-01</th>
      <td>3.99</td>
      <td>2.90</td>
      <td>2.17</td>
    </tr>
    <tr>
      <th>2011-05-01</th>
      <td>5.29</td>
      <td>4.39</td>
      <td>3.88</td>
    </tr>
    <tr>
      <th>2011-11-01</th>
      <td>4.59</td>
      <td>4.14</td>
      <td>4.35</td>
    </tr>
  </tbody>
</table>

Om vi gör en graf över räntorna över tid blir det:

![Ränta över tid](/images/2018/03/rates.png)

Man kan se en tydlig topp i den rörliga räntan då riksbanken satte
styrräntan till 500% (bolån nådde "endast" 24%). Man kan också se att
under tidigt 90-tal var den rörliga räntan under relativt långa
tidsperioder högre än den bundna. Men för att jämföra den faktiska
kostnaden över bindningstiden måste vi jämföra genomsnittsräntan.

Till exempel, låt oss jämföra faktiska genomsnittsräntorna från förste
juli 1991 i 5 år för rörlig ränta (11.96%) och 5 års bunden ränta
(12.25%).  Trots att man med rörlig ränta under 3 månader hade 24% i
ränta så skulle man under 5 år betalat mindre totalt.

Om samma beräkning utförs för varje månad kan man se hur mycket man
hade tjänat/förlorat beroende på när man valde att binda. Eftersom 5
år inte är jämnt delbart med 2 år så avser 2 års räntan vad din
genomsnittsränta hade varit de första 5 åren av 6.

![Medelränta under 5 år](/images/2018/03/5y_avg_rates.png)

Det är ganska tydligt att rörlig ränta nästan alltid varit det mest
lönsamma alternativet. Vid tre tidspunkter har det varit mer lönsamt
att binda i 5 år: slutet av 1989, början av 1997 och mitt i 2005. 2
års räntan kommenterar jag inte eftersom det inte är en rättvis bild
att bara titta på 5 av 6 år.

Om vi jämför 2 års bindingstid med rörlig ränta:

![Medelränta under 2 år](/images/2018/03/2y_avg_rates.png)

Även här har rörlig ränta mestadels varit mest lönsamt men att binda
räntan har varit lönsamt under flera perioder när räntan varit
stigande. Den viktiga skillnaden mot 5 års bindningstid är att man
inte är låst så länge när räntan väl börjar gå ner igen (och kan byta
till rörlig ränta).

Om vi jämför under 10 år:

![Medelränta under 10 år](/images/2018/03/10y_avg_rates.png)

Här är det tydligt att den rörliga räntan är mest fördelaktig.

# Slutsats

Även om det vid väl valda tillfällen (29 år och endast 3 korta
tillfällen!) varit möjligt att binda sitt lån i 5 år och betala mindre
totalt än med rörlig ränta anser jag att det är på tok för osannolikt
att man lyckas pricka ett sådant tillfälle. Det är nästan garanterat
att man kommer att betala mer i slutändan.

Att binda sitt lån i 2 år är mer troligt att man lyckas tjäna på, men
även här är det större risk att man i slutändan betalar mer. Notera
att förutom under krisen i början av 90-talet så har den rörliga
räntans möjliga nedsida varit betydligt mildare än den bundna räntans
möjliga nedsida. Notera avståndet mellan 2 års bunden ränta och den
rörliga räntan, när det totalt blivit dyrare med den rörliga räntan
har det inte varit nära nog så mycket dyrare som när den bundna räntan
varit dyrare.

Man kan lätt tro från dom här graferna att det är lätt att se när man
skulle tjäna på att binda sitt lån, men notera att dom dras med ett
lagg om 2/5/10 år. Korrelationen med ögonblicksbilden av räntan
(vilket är allt du har att gå på i verkligheten) är väldigt låg.

Så slutsats: håll dig till rörlig ränta och var trygg med att du var
tredje månad vanligtvis gör det mest lönsamma valet.

[1]: http://hypotek.swedbank.se/rantor/historiska-rantor/
[2]: https://gitlab.com/spacecowboy/swedish-interest-rates
[3]: https://gitlab.com/spacecowboy/swedish-interest-rates/raw/master/swedish_interest_rates.csv
