+++
date = "2017-04-03"
draft = true
title = "Keybindings in Gnome"
slug = "keybindings-in-gnome"
tags = ['linux']
banner = ""
aliases = ['/keybindings-in-gnome/']
+++

Gnome is stupid...

Go through and disable basically all default keys, by rebinding to
shit you won't use because you can't simply delete keybindings...

Then you need some special fixes:

## Super-P

For `Super-P` install `dconf-tools` and then run `dconf-editor`.

Navigate to `/org/gnome/settings-daemon/plugins/xrandr/` and turn it off. But this is not enough.

Do not listen to advice on the [internet](http://askubuntu.com/questions/68463/how-to-disable-global-super-p-shortcut)
about disabling media keys.

Instead, search for `<Super>p` and disable any uses of it.

Repeat for any other keys you are interested in.

Finish by adding your custom keyboard shortcut in regular keyboard settings.

## Alt-Shift

https://www.guyrutenberg.com/2015/10/02/gnome-altshift-and-altshifttab/

> Start the Gnome’s Tweak Tool and select Typing from the Tweaks menu. Under “Miscellaneous compatibility options” select “Shift cancels Caps Lock”.
