+++
banner = ""
categories = []
date = "2016-09-28T22:57:21+02:00"
description = "How to reboot your machine after a thief enters the wrong password using PAM."
draft = false
images = []
menu = ""
series = []
tags = ["linux","computers","encryption","security"]
title = "Rebooting on wrong password"
+++

Having an encrypted hard drive is all well and good, but chances are
that if someone is gonna steal your laptop, it's probably not going to
be turned off. Most likely, it will be stolen in a powered-on
state. And so your encrypted hard drive doesn't increase your security
at all since it's currently unlocked.

In my mind, it's a slight improvement if the computer somehow can
shutdown if someone is trying to gain access to it. That way, the hard
drive is no longer accessible and the number of possible attack
vectors go down drastically. And so, if you type the wrong password 3
times on my laptop, it shuts down.

This is accomplished by using `PAM`, and its ability to invoke an
arbitrary script as part of the login flow via `pam_exec.so`. The
script itself looks like this:

```shell
#!/bin/bash
# Do not add -eu, you need to allow empty variables here!

# To be used with PAM. Look in /etc/pam.d for the script that your
# screensaver etc uses. Typically it references common-account and common-auth.
#
# In common-auth, add this as the first line
#auth       optional     pam_exec.so debug /path/to/wrongpassword.sh
#
# In common-account, add this as the first line
#account    required     pam_exec.so debug /path/to/wrongpassword.sh
#

COUNTFILE="/var/log/failed_login_count"

# Make sure file exists
if [ ! -f "${COUNFILE}" ];then
  touch "${COUNTFILE}"
  chmod 777 "${COUNTFILE}"
fi

# Read value in it
COUNT=$(cat "${COUNTFILE}")
# Increment it
COUNT=$((COUNT+1))
echo "${COUNT}" > "${COUNTFILE}"

# if authentication
if [ "${PAM_TYPE}" == "auth" ]; then
  # The count will be at 4 after 3 wrong tries
  if [ "${COUNT}" -ge 4 ]; then
    # Shutdown in 1 min
    #/usr/bin/shutdown --no-wall -h +1
    # This is a hack because the line above gives a segfault in logind
    echo "0" > "${COUNTFILE}"
    systemctl poweroff
  fi
# If authentication succeeded, and we are now in account phase
elif [ "${PAM_TYPE}" == "account" ]; then
  echo "0" > "${COUNTFILE}"
  # Cancel shutdown which was just issued
  shutdown -c
fi

exit 0
```

On my Debian system, PAM ends up looking at `/etc/pam.d/common-auth`
and `/etc/pam.d/common-account`. These are invoked in different parts
of the authentication flow. In `common-auth`, add this as the first
line:

```text
auth optional pam_exec.so debug /path/to/wrongpassword.sh
```

And then in `common-account`, add this as the first line:

```text
account required pam_exec.so debug /path/to/wrongpassword.sh
```

You can try it immediately if it works. Lock your screen, and type the
wrong password 4 times. If it works, your computer should shut down.

## WARNING: DO NOT ENABLE ON SERVERS

This is __NOT__ something you want to do on any machine. Most notably,
it's probably a huge mistake to copy this verbatim on a machine which
accepts remote connections. In that case, you essentially enable
anyone to DOS you by entering the wrong password via SSH or
similarly. So don't do this if you allow remote connections to your
machine (which shouldn't be a thing on a laptop).
