
+++
date = "2014-04-25"
draft = true
title = "Start contributing to open source"
slug = "start-contributing-to-open-source"
tags = []
banner = ""
aliases = ['/start-contributing-to-open-source/']
+++

# Use open source; switch to Linux
This is not just sound advice for getting a sane development environment. If you want to use open source then Linux (or a BSD flavor) is the only way. Windows and OS X both have open source software available of course, but most of your apps will be closed source.

# Learn git

# Start small
Fix typos in the interface. Tweak theme settings.

# Ask yourself: how could this improved?"
Then see if you can make it happen.

