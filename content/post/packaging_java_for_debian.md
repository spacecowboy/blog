+++
date = "2017-10-01"
draft = true
title = "Packaging Java for Debian"
slug = "packaging-java-for-debian"
tags = []
banner = ""
aliases = ['/packaging-java-for-debian/']
+++


## Neo4j parents

- Clone source source repo
- Create a branch and call it `debian` or something
- Run `mh_make` which will generate the debian directory
- Commit debian directory to your debian branch
- Run `git-buildpackage` like so:

```
gbp buildpackage --git-upstream-branch=master --git-debian-branch=debian --git-upstream-tag='%(version)s'
```

assuming then of course then original source branch was `master`, your
debian branch is `debian`, and that tags are "raw" version numbers.

Then install the resulting debian package in your system so you can
repeat this for Neo4j.

You will see pom files installed on system maven repo:

```
$ tree /usr/share/maven-repo/org/neo4j
/usr/share/maven-repo/org/neo4j
└── build
    ├── assembly-pom
    │   ├── 42-SNAPSHOT
    │   │   └── assembly-pom-42-SNAPSHOT.pom
    │   └── debian
    │       └── assembly-pom-debian.pom
    ├── examples-assembly-descriptor
    │   ├── 42-SNAPSHOT
    │   │   ├── examples-assembly-descriptor-42-SNAPSHOT.jar -> ../../../../../../java/examples-assembly-descriptor.jar
    │   │   └── examples-assembly-descriptor-42-SNAPSHOT.pom
    │   └── debian
    │       ├── examples-assembly-descriptor-debian.jar -> ../../../../../../java/examples-assembly-descriptor.jar
    │       └── examples-assembly-descriptor-debian.pom
    ├── examples-pom
    │   ├── 42-SNAPSHOT
    │   │   └── examples-pom-42-SNAPSHOT.pom
    │   └── debian
    │       └── examples-pom-debian.pom
    ├── grandparent
    │   ├── 42-SNAPSHOT
    │   │   └── grandparent-42-SNAPSHOT.pom
    │   └── debian
    │       └── grandparent-debian.pom
    ├── parent-central
    │   ├── 42-SNAPSHOT
    │   │   └── parent-central-42-SNAPSHOT.pom
    │   └── debian
    │       └── parent-central-debian.pom
    └── parent-pom
        ├── 42-SNAPSHOT
        │   └── parent-pom-42-SNAPSHOT.pom
        └── debian
            └── parent-pom-debian.pom
```

## Neo4j

- checkout 3.2.5
- checkout new branch `debian/3.2`
- set version to 3.2.5 `mvn versions:set -DnewVersion=3.2.5`
- commit
- run `mh_make`


### Dependencies needed

- `liblucene4.10-java` needs to have `5.5.0` available
- https://github.com/google/auto/tree/master/service
- scala-maven-plugin 3.2.0
