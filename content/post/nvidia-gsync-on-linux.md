
+++
date = "2016-03-05"
draft = false
title = "Nvidia G-Sync and Linux"
slug = "nvidia-gsync-on-linux"
tags = ['gaming', 'linux']
banner = "/images/2016/03/NVIDIA-X-Server-Settings_007-1.png"
aliases = ['/nvidia-gsync-on-linux/']
series = ["Linux 100Hz gaming"]
+++

After getting a fancy new monitor with G-Sync support, I was eager to try it out in my Linux gaming setup. While Nvidia fully supports G-Sync in their Linux drivers, it turns out that other components of the system can get in the way. As explained by a [post on the Nvidia forums](https://devtalk.nvidia.com/default/topic/854184/gsync-is-not-working/?offset=1):

> For G-SYNC to work, the application has to be able to flip and the symptoms you're describing here sound like it's not able to flip in your configuration. There are a variety of reasons why flipping might not be working, but the most likely culprits here are either the compositor getting in the way, or the game not being completely full-screen. The full-screen requirement includes the game being completely unoccluded, so if your window manager is drawing something on top of the game, even just by one pixel, it will prevent flipping. Full-screen also means that it has to cover the entire X screen, which includes both monitors if you have them both enabled.
>
>Can you please try a different window manager / desktop environment to see if the behavior changes?

Since only a minority of PC-gamers are actually on Linux, and only a minority of those actually have G-Sync capable monitors, Googling for assistance was... challenging. So, for any other Linux gamers out there, here is a short guide on how to enable G-Sync and verify that it works. Some of the steps are XFCE specific, as this is my window manager of choice on my gaming PC. If you are using a different window manager, you'll have to look through your options to find the equivalent settings.

## Nvidia settings

- Sync to VBlank: Optional
- Allow Flipping: Required
- Allow G-SYNC: Required
- Enable G-SYNC Visual Indicator: Optional

The only two required settings are _flipping_ and _G-Sync_, the others are optional. Enabling _Sync to VBlank_ (VSync) in combination with G-Sync only prevents the GPU from generating an FPS beyond your monitor's max refresh rate (which you can't see anyway). It is turned off below the max refresh rate when G-Sync is enabled.

The visual indicator is useful here to see that G-Sync is working. If all goes well, you should see a green "G-SYNC" text in the corner when running a game.

![Nvidia settings](/images/2016/03/NVIDIA-X-Server-Settings_007.png)

## Disable compositor

As mentioned in the forum post, a compositor will prevent G-Sync from activating because essentially something is rendering above the game. The same reason prevents G-Sync from working in Window mode (unlike Windows, where G-Sync does not require fullscreen).

For XFCE, go to _Window Manager Tweaks_ under _Settings_
![XFCE Settings](/images/2016/03/Selection_004.png)

Then under the _Compositor_ tab, make sure the compositor is disabled
![Window Manager Tweaks](/images/2016/03/Selection_005.png)

In addition, depending on your setup, make sure you don't have things like [Compton](https://wiki.archlinux.org/index.php/Compton) or [Compiz](https://wiki.archlinux.org/index.php/Compiz) enabled.

## Start a game  in fullscreen

As mentioned, you must run the game in fullscreen mode. G-Sync does not work with window mode in Linux.

I did notice that there are games which do not enable G-Sync. One example is "Cities: Skylines". So make sure to try several games if you don't see the G-Sync logo.

A good candidate here is Dota 2 since it is free to play. Dota 2 running in "Desktop-Friendly Fullscreen" does enable G-Sync. As does Portal 2 and XCOM 2.
