
+++
date = "2014-04-07"
draft = false
title = "Getting Adblock to work in Conkeror"
slug = "getting-adblock-to-work-in-conkeror"
tags = ['conkeror', 'adblock']
banner = ""
aliases = ['/getting-adblock-to-work-in-conkeror/']
+++

Conkeror supports firefox addons to varying degrees. I found that a good indicator is if the addon has support for Firefox 3. This means you can use Adblock 2.0. But, the GUI for selecting a filter subscription will not show.  Hence the need to install Adblock 1.3 **first**. To get Adblock up and running in Conkeror, do the following:

1.  In your rc-file, set:
```javascript
session_pref("xpinstall.whitelist.required", false);
```
2.  Go to [Adblock versions](https://addons.mozilla.org/en-US/firefox/addon/adblock-plus/versions/).
3.  Install **1.3.10**.
4.  Open extensions: `M-x extensions`.
5.  Go into preferences for Adblock and subscribe to a list, like
    *Easylist*. The list might complain about requiring Adblock 2 for
    some filters, which is fine since we will fix that next.
6.  Now go back and download/install version **2.0.1**.
7.  Enjoy the web again.


