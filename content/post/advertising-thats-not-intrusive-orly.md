
+++
date = "2014-04-07"
draft = false
title = "Advertising, that's not intrusive. Orly?"
slug = "advertising-thats-not-intrusive-orly"
tags = ['android', 'advertising']
banner = ""
aliases = ['/advertising-thats-not-intrusive-orly/']
+++

When you have apps in Google Play (and I imagine, other App stores as well), the amount of spam you receive instantly goes up by a factor of 10. Google's spam filters are pretty well trained but every now and again something gets through.

## Advertising opportunity

Today's piece of bullshyt (I really meant to spell it like that) reads as follows (my emphasis):

>Our premium advertisers are currently looking to buy android traffic at a very high price in apps like Nononsense Notes.
>
> We think you can generate up to $10 CPM with their **full screen ads**, which are **very clean**. Indeed, most of our advertisers are willing to pay, on average, between $1 and $3 per installation. You're free to display these ads whenever you want in your app so that it's **not intrusive**.

Ads are by definition _intrusive_. That's how they nag you into buying their stupid stuff. And it doesn't matter how clean your ads are. Displaying them fullscreen is _beyond_ intrusive. It is down right __offensive__.

I uninstall anything that displays obnoxious ads, be they fullscreen or notifications, and promptly give the app a one star review. I sincerely hope others afford me the same "courtesy" for my apps.

