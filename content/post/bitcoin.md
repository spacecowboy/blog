
+++
date = "2014-05-16"
draft = true
title = "Bitcoin"
slug = "bitcoin"
tags = []
banner = ""
aliases = ['/bitcoin/']
+++

# Internet
No one predicted the big stuff

It would sound weird. What's the use?

* Wikipedia
* Napster -> Torrents
* Streaming Music Videos
* Death of TV

# Bitcoin

## Money on the Internet
Not new by itself

## Free transactions
Threat to credit card institutions as payments will become cheaper and cheaper

## Decenterelized shadow banking
Will not mean the death of banks. As it is, shadow banking is already eating into the bank's privileged domains. This will fit nicely into shadow banking however.

## Programmatic money
This is the real innovation and the thing that no one can predict what will come of it.
Some ideas though:

* Peer-to-peer funded banks
* Peer-funded venture capitalism
* Decentrelized corporations. Non-evil by design.
* Mob-rule salary payments
* Escrow service
* Receipts built into the transaction, saved in the blockchain, encrypted so only the holder of the wallet can read it

## Links
[https://en.bitcoin.it/wiki/Contracts](https://en.bitcoin.it/wiki/Contracts)

