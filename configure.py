#!/usr/bin/env python

"""
Script that generates the ninja build file.
"""

import os
import sys


ninja_file = """
rule configure
  command = ./configure.py
  generator = 1

build build.ninja: configure | configure.py

rule zopfli
  command = ./zopflipng $in $in && touch $out
  generator = 1

rule hugo
  command = hugo --cleanDestinationDir

rule rsync
  command = rsync -avzr --progress --delete public/ linode:"/var/www/blog/public" && touch $out
"""

build_zopfli = """
build {outfile}: zopfli {infile}
"""


build_hugo = """
build public/index.html: hugo | {inputs} {zopfli_sentinels}

build public: phony public/index.html

build .rsync_sentinel: rsync | public

build rsync: phony .rsync_sentinel

default public
"""


def add_hugo_statement(build_file, hugo_inputs, zopfli_sentinels):
    """
    Adds a rule to build the public directory, depending on all the
    inputs.
    """
    return "{}{}".format(
        build_file,
        build_hugo.format(inputs=" ".join(hugo_inputs),
                          zopfli_sentinels=" ".join(zopfli_sentinels))
    )

def find_hugo_inputs(root):
    """
    Returns a list of all files relevant for running Hugo.
    """
    files = []

    files.append(os.path.join(root, "config.toml"))

    for folder in ["archetypes",
                   "content",
                   "data",
                   "resources",
                   "static",
                   "themes"]:
        for (path,
             dirnames,
             filenames) in os.walk(os.path.join(root,
                                                folder)):
            files.extend([
                os.path.join(path, fname) for fname in filenames
            ])

    return files


def zopfli_sentinel(png):
    """
    Returns the filename of the zopfli sentinel for the given png
    """
    dirname, fname = os.path.split(png)
    
    return os.path.join(dirname,
                        ".{}.zopfli".format(fname))


def optimize_png(build_file, png):
    """
    Appends a build statement to optimize the given png file.
    """

    sentinel = zopfli_sentinel(png)
    
    return "{}{}".format(
        build_file,
        build_zopfli.format(infile=png,
                            outfile=sentinel)
    )

def find_pngs(start_path):
    """
    Recursively finds all PNG files below start_path
    """
    pngs = []
    for (path, dirnames, filenames) in os.walk(start_path, followlinks=True):
        for filename in filenames:
            if (filename.endswith(".png")
                or filename.endswith(".PNG")):
                pngs.append(os.path.join(path, filename))

    return pngs

pngs = find_pngs("static/images")

for png in pngs:
    ninja_file = optimize_png(ninja_file, png)

hugo_inputs = find_hugo_inputs(".")

ninja_file = add_hugo_statement(
    ninja_file,
    hugo_inputs,
    [zopfli_sentinel(png) for png in pngs]
)
    
with open("build.ninja", "w") as NF:
    print(ninja_file, file=NF)
