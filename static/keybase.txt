==================================================================
https://keybase.io/spacecowboy
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://cowboyprogrammer.org
  * I am spacecowboy (https://keybase.io/spacecowboy) on keybase.
  * I have a public key with fingerprint 3270 DC43 5A7D 604F 5B06  3521 987C 54AB 0D44 51ED

To do so, I am signing this object:

{
    "body": {
        "key": {
            "eldest_kid": "0101769533adf10a67e13fba6ba84897749d789e5954cc29ea36efc0c22154dfcbd00a",
            "fingerprint": "3270dc435a7d604f5b063521987c54ab0d4451ed",
            "host": "keybase.io",
            "key_id": "987c54ab0d4451ed",
            "kid": "0101769533adf10a67e13fba6ba84897749d789e5954cc29ea36efc0c22154dfcbd00a",
            "uid": "7f30f2b6588e5ed16f1aaa26f8a08319",
            "username": "spacecowboy"
        },
        "revoke": {
            "sig_ids": [
                "1f8045ad2104fa3980ac853665e0cdfb8ce63e8f5c2aa4042ca8767631eb5d010f"
            ]
        },
        "service": {
            "hostname": "cowboyprogrammer.org",
            "protocol": "https:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1480418033,
    "expire_in": 157680000,
    "prev": "8c6214079a625b14aff1d9944d23dbc1d0fb5bb0cefeacf5f8e904e0a8e0b876",
    "seqno": 5,
    "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: GnuPG v1

owGtkntQVFUcx5clJRhNMciYISZuagkMnPu+dxvkmSuJIq+BCXaWc+89d/e2srve
XYENyEcKw/ignFAa02RIxoEckYlHmolATQUqYU08jAzBZEJjClFmQrpL9l9/dv45
c37z/X1/n9/v/N5d7qvT+9SWZTUtT06/59M7KehyY4X5UkxwSB7MUIrZ0OKFtkvI
5TbbFAkzYAAHOMvwNElCScYBZFiEk7IAGQFyFMezLMVLLMcjmqcpUSR4BEkGySIQ
CQKnKUkWBQkAiEVhsmK3INWpKna3ZksSLJBEiqQhKzGAkmkBMCRN4DzHijQFBSBR
FI0jSUu0OlzeDA1OgC4UrTi0mPYwL+L9h/5/5t65aMfKJJAJgaE5DtFIwhkZhxAS
jMxBwJE47xW6kGqHhUhTu5xQRKKjWHB4sPIoTEVFDhvyjtalWDRuF2bIw3CZAxQN
JQLX2ockzwEocjTJMDQCoiQLnIgYEnEyLRIQUoAiRMixDMuQOBJoSWtPxkyat1a0
SBEXzb2DegLwT22n6rCosLAQqdEO1aIhagG3Q3Rs1xRWt9t52WXw4rk9Tm9OMRLM
T9zMgmKXtP/SUoqQ6lIcdsyAa0rRrXjtcUpDxzlAklEYKnEqKjIrXgXNMhzQjrcQ
KtIsOZEhcAqwPGQIWsApKMu4xPMUJRGkJIi4BGSBFgQgIhlBUaZlDvGAQgByCAha
t5i3vx12B2agNUxo8Y5Wsdihe6eKsPKAKn3oUzofvW7pEr13kXUB/iv/3e5z9SsW
UFiBrvJKYP1oYI3t2qGk8FffOTH1zMH+0OdtZnlV9oby0uGUnJ/iRvJDC5ZNG1dX
vNwdtMkUMhazP6rxOMsVv2kaz6uJ28zkTDaHtHfXJb5Ulzg12PdH2NWH1obp27+f
tE4tGDJ8B//8pmCXsYuhc5KvD+x4bEBnEwfY5La0Y7OpweUX2wpy8R+eTsg6l9cc
2VqedLSWzMJnxhOTLywEBahzqVU1UnuKQI03jfkMbXy7YWv5XGxKxDGfJYfbtqwY
revwxKZ8unvfXxGD5vHB/PbV0++t68zOY3xTA+sjNvodTOrrXevxHznak+5//0TP
A/HF9NtLuzcMj86s/96a8Pqy802e+PiqM2d+efDVG7oDE2EtZZ8Pp20lrXs/WqFP
GgmJmVCM10duDkRW2xZKemPW5wS2/XijdZezZcBYURZxZDC4LCHjinOdPWdlp1rV
MDFHzWdsCfpk+tH72V1xk9mzROhbwVjr3Ndz+K0uy9o93+nnp3575Q7WcXf2/tmf
n3V+effUw8bH6bpwQ2t13HOZn0XGpLkbE6ozD3z74amqDy7GH668M7rvV9PV5kvF
1pacNaNdMzes53dvazJ83G/xi8ZbHt2aHoswXZj3mzjkNJ7crzsCSvo2v0bM7an+
4oXj99J4S9+lzKBrVbU3Y0tP568arFgTPt4Z3W4aqtw71NFv7DndFZO7advlvwE=
=gOgy
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/spacecowboy

==================================================================


